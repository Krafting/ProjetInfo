# Our web project Gitlab repository

This project is developed under the GPLv3 licence, feel free to fork or make merge requests as you like!

# FUTURE IDEAS:
* Commandes: faire une table avec les départements pour pas que les utilisateurs rentre n'importe quoi
* Refaire la fonction getcurrenturiargs()
* Admin page des plays: ajouter le play sur la page d'accueil

# KNOWN BUGS:

* Les commentaires dans les fichiers admins sont un peut mal fais

# BRANCHES

* /master : MySQL Support, the working branch
* /postgresql : Old PostgreSQL Support, Ce qu'on a monter lors de la présentation
* /mysql : MySQL Support, Pas vraiment maintenu, j'utilise plutot la branch /master, mais je pusherais quelques trucs de temps en temps la bas

# A ne pas oublier avant de push:

* Retirer la bdd dans include/init.php
* Retirer le secret captcha dans include/forms/RegisterForms.php
* Faire attention au README