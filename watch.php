<?php
$pageTitle = "Plays";
$isPlay = true;
include('include/init.php');
include('include/header.php');

/* SI UN PLAY A ÉTÉ DEMANDER */
if(isset($_GET['uuid']) AND !empty($_GET['uuid']) AND is_numeric($_GET['uuid'])) {


    /* ON CHECK SI LE PLAY EXISTE */
    $findPlay = $connexion->prepare('SELECT COUNT(*) FROM plays WHERE idvideo=:idvideo');
    $findPlay->execute(array(
        'idvideo' => $_GET['uuid']
    ));
    $check = $findPlay->fetch();

    if($check[0] > 0) {
        /* ON SELECTIONNE LE PLAYS DEmANDER */
        $selectPlay = $connexion->prepare('SELECT * FROM plays INNER JOIN users ON refuser=iduser INNER JOIN typevideo ON reftypevideo=idtypevideo WHERE idvideo=:idvideo');
        $selectPlay->execute(array(
            'idvideo' => $_GET['uuid']
        ));
        $play = $selectPlay->fetch();


        /* GESTION DE LA SUPPRESSION DE COMMENTAIRE */
        if(isset($_GET['del']) AND !empty($_GET['del']) AND is_numeric($_GET['del'])) {
            $findIfUser = $connexion->prepare('SELECT COUNT(*) FROM commentairesplay WHERE refusersent=:iduser AND idcommentaireplay=:idcomm AND refplay=:uuid');
            $findIfUser->execute(array(
                'iduser' => $_SESSION['id'],
                'idcomm' => $_GET['del'],
                'uuid' => $_GET['uuid']
            ));
            $count = $findIfUser->fetch();
            echo $count[0];
            /* SI ON EST L'AUTEUR DU COMMENTAIRE, SI ON EST ADMIN, OU SI LE PLAY NOUS APPARTIENT */
            if($count[0] > 0 OR isAdmin() OR $play['refuser'] == $_SESSION['id']) {
                $deleteComment = $connexion->prepare('DELETE FROM commentairesplay WHERE idcommentaireplay=:idcomment');
                $deleteComment->execute(array(
                    'idcomment' => $_GET['del']
                ));
                header('Location: watch.php?uuid='.$_GET['uuid'].'&succ=80');
                exit();
            } else {
                header('Location: watch.php?uuid='.$_GET['uuid'].'&err=78');
                exit();
            }
        }

        /* ON CHECK SI L'IP A VUE LA VIDEO DANS LES DERNIERES 24 H*/ 
        $selectLastView = $connexion->prepare('SELECT COUNT(*) FROM viewplay WHERE ip=:ip AND refplay=:refplay AND dateview=:dateview');
        $selectLastView->execute(array(
            'refplay' => $_GET['uuid'],
            'ip' => $_SERVER['REMOTE_ADDR'],
            'dateview' => date('Y-m-d')
        ));
        $count = $selectLastView->fetch();

        /* SI NON, ON AJOUTE LA VUE */
        if($count[0] == 0) {
            /* ON AJOUTE LA VUE */
            $addView = $connexion->prepare('INSERT INTO viewplay (refplay, ip, dateview) VALUES (:refplay, :ip, :dateview)');
            $addView->execute(array(
                'refplay' => $_GET['uuid'],
                'ip' => $_SERVER['REMOTE_ADDR'],
                'dateview' => date('Y-m-d') 
            ));
        }
?>
    <div class="content" style="width: 1250px;">
        <div class="page viewplays">
            <?php 
            if(isset($_GET['err']) OR isset($_GET['succ'])) {
                if(!empty($_GET['err']) AND is_numeric($_GET['err'])) {
                    $idMsg = $_GET['err'];
                    echo getMessage($idMsg);
                    echo "<br>";
                }
                if(!empty($_GET['succ']) AND is_numeric($_GET['succ'])) {
                    $idMsg = $_GET['succ'];
                    echo getMessage($idMsg);
                    echo "<br>";
                }
            } ?>
            <div class="flexBlock">
                <div class="flexContent" style="padding: 0;flex: 2; margin-bottom: 50px;">
                    <video style="background: var(--dark-grey);" width="100%" height="500" controls>
                        <source src="upload/plays/<?php echo $play['iduser']; ?>/<?php echo $play['nomfichiervideo']; ?>" type="video/mp4">
                        <source src="movie.ogg" type="video/ogg">
                        Votre navigateur ne supporte pas les vidéos, téléchargez <a href="https://librewolf-community.gitlab.io/">LibreWolf</a>!
                    </video>

                    <h1 style="margin: 5px 0 15px 0;text-align: left"><?php echo $play['titrevideo']; ?></h1>
                     
                    <div class="userBlockVid">
                        <img src="upload/profiles/<?php echo findProfilePic($play['refuser']); ?>"></img>
                        <p>
                            <a href="profiles.php?id=<?php echo $play['iduser']; ?>"><b><?php echo $play['pseudo']; ?></b> <br> 0 abonné</a>
                            <br><p style="float:right"><?php echo findNumViews($play['idvideo']); ?> vues</p>
                        </p>
                    </div>
                    <div class="videoDescription" style="text-align: left;margin-top: 150px;    word-break: break-all;">
                        <h2>Description</h2>
                        <style>
                            #more {display: none;}
                        </style>

                        <?php if(strlen($play['descriptionvideo']) > 999) { ?>
                            <p><?php echo truncage(0, $play['descriptionvideo'], 1000); ?><span id="dots"> ...</span><span id="more"><?php echo truncage(1000, $play['descriptionvideo'], strlen($play['descriptionvideo'])); ?></span></p>
                            <button onclick="showText()" id="myBtn">Afficher plus</button>
                        <?php } else { ?>
                            <p><?php echo $play['descriptionvideo']; ?></p>
                        <?php } ?>
                        <h2>Catégorie : <?php echo $play['nomtypevideo']; ?></h2>
                                                
                    </div><hr style="margin-top: 40px;">
                <div id="CommentairesBlock" class="tabContent">
                    <h2 style="text-align:left;">Commentaires (<?php echo findNumPlayComment($play['idvideo']); ?>)</h2>
                    <?php if(connect()) { ?>
                    <div class="sendComment" style="margin-bottom: 110px;">
                        <form action="include/forms/addCommentPlay.php" method="post">
                            <textarea style="margin: 0; width: 96%;" name="contentComment" placeholder="Vous pouvez laisser un commentaire sur ce Play"></textarea>
                            <input type="hidden" name="idreceived" value="<?php echo $_GET['uuid']; ?>">
                            <button type='submit' style="float:right;" name="sendComment" class="btn">Envoyer mon commentaire!</button>
                        </form>
                    </div>
                    <?php }  else { echo '<div class="infoMessage"><a href="login.php">Connectez-vous pour laisser un commentaire.</a></div>'; }
                    /* ON RECUPERE LES COMMENTAIRES QUE L'UTILISATEUR A RECU SUR LE PLAY */
                    $reqSelectComment = $connexion->prepare('SELECT * FROM commentairesplay INNER JOIN users ON refusersent=iduser WHERE refplay=:id ORDER BY idcommentaireplay DESC');
                    $reqSelectComment->execute(array(
                        'id' => $_GET['uuid']
                    ));
                    $comments = $reqSelectComment->fetchAll();
                    
                    $count = count($comments);
                    if($count > 0) {
                    foreach($comments as $comment) {
                    ?>
                    <div class="avisArticles">
                        <div class="avisArticle" style="margin-bottom: 25px;">
                                <div style="flex-flow:column;">

                                <div class="flexBlock oneChild">
                                    <div class="flexContent" style="flex: 1; padding: 10px;max-width: 100px;">
                                        <div class="profileBlock" style="max-height: 100px;">
                                            <img style="border-radius: 10px" src="upload/profiles/<?php echo findProfilePic($comment['iduser']); ?>"></img>
                                        </div>
                                    </div>
                                    <div class="flexContent" style="flex: 6; padding: 10px; text-align: left;">
                                        <a style="margin: 0;" href="profiles.php?id=<?php echo $comment['iduser']; ?>">
                                            <h2 style="margin:0;"><?php echo $comment['pseudo']; ?>
                                            <?php if((connect() AND $comment['iduser'] == $_SESSION['id']) OR isAdmin() OR (connect() AND $play['refuser'] == $_SESSION['id'])) { ?>
                                                <a href="<?php echo '?'.findCurrentURIArgs($_SERVER['REQUEST_URI']).'&del='.$comment['idcommentaireplay']; ?>" style="float:right"><i class="fas fa-trash-alt"></i></a>
                                            <?php } ?>
                                            </h2>
                                        </a>
                                        <p style="margin:0; padding-bottom: 20px;"><?php echo date("d M Y à h:i:s", $comment['timecommentaireplay']); ?></p>
                                        
                                        <?php if(strlen($comment['textecommentaireplay']) > 999) { ?>
                                            <p style="word-break: break-all;"><?php echo truncage(0, formatTexte($comment['textecommentaireplay']), 1000); ?><span id="dots"> ...</span><span id="more"><?php echo truncage(1000, $comment['textecommentaireplay'], strlen($comment['textecommentaireplay'])); ?></span></p>
                                            <button onclick="showText()" id="myBtn">Afficher plus</button>
                                        <?php } else { ?>
                                            <p style="word-break: break-all;"><?php echo formatTexte($comment['textecommentaireplay']); ?></p>
                                        <?php } ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php } } else { echo '<div class="infoMessage" style="margin-top: 150px;">Ce Play n\'a pas encore reçu de commentaire.</div>'; } ?>
            
                </div>
                </div>

                <div class="flexContent" style="padding: 0;flex: 1;">

                    <div class="flexPlayList">
                        <?php 
                        /* ON SELECTIONNE DES PLAYS A METTRE DANS LA SIDEBAR */
                        $searchAllPlays = $connexion->prepare('SELECT * FROM plays INNER JOIN users ON refuser=iduser LIMIT 20');
                        $searchAllPlays->execute();
                        
                        foreach($searchAllPlays as $playList) {
                        ?>
                            <div class="previewPlay">
                                <a href="watch.php?uuid=<?php echo $playList['idvideo']; ?>">
                                    <img src="upload/thumbs/<?php echo $playList['refuser'].'/'.$playList['miniature']; ?>"></img>
                                </a>
                                <div class="userBlock">
                                    <p >
                                        <a href="watch.php?uuid=<?php echo $playList['idvideo']; ?>"><b><?php echo $playList['titrevideo']; ?></b></a>
                                        <br><a href="profiles.php?id=<?php echo $playList['iduser']; ?>"><?php echo $playList['pseudo']; ?></a> - <?php echo findNumViews($playList['idvideo']); ?> vues
                                    </p>
                                </div>
                            </div>
                        <?php } ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

<?php
    } else {
        header('Location: plays.php');
        exit();
    }
} else {
    header('Location: plays.php');
    exit();
}
include('include/footer.php');
?>
