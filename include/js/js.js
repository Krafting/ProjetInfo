/* LES MODALS */ 
function openModal(divModal) {
  // Get the modal
  var divModal = document.getElementById(divModal);

  // Get the <span> element that closes the modal
  var span = document.getElementsByClassName("close");

  // When the user clicks on the button, open the modal
  divModal.style.display = "block";

  // When the user clicks on <span> (x), close the modal
  for (let i = 0; i < span.length; i++) {
    console.log(span[i]);
    span[i].onclick = function() {
      divModal.style.display = "none";
    }
  };

  // When the user clicks anywhere outside of the modal, close it
  window.onclick = function(event) {
    if (event.target == divModal) {
      divModal.style.display = "none";
    }
  }
}

/* FONCTION POUR SELECTIONNER LES ANCIENNES IMAGES DE L'USER */
function selectPicture(variab) {
    inputHiddenSelect = document.getElementById("selectImageName");
    inputHiddenDelete = document.getElementById("selectImageNameDelete");
    inputHiddenDelete.value = variab;
    inputHiddenSelect.value = variab;
}

/* COMPTE LE NOMBRE DE CARACTERES DANS UNE TEXTAREA UTILISER DANS LA DESCRIPTION UTILISATEUR */
function countChar() {
  document.getElementById('count').innerHTML = "" + (document.getElementById('textareaCount').value.length) + " / 2000";
}

/* AFFIN DE COPER DES CHOSES VIA DES LIENS (Pages wishlist.php) */
function copy(text) {
  var dummy = document.createElement("textarea");
  document.body.appendChild(dummy);
  dummy.value = text;
  dummy.select();
  document.execCommand("copy");
  document.body.removeChild(dummy);
}

/* FONCTION POUR LES TABS SUR LES PROFILES UTILISATEURS */ 
function openTab(tab, menu) {
  var i;
  var x = document.getElementsByClassName("tabContent");
  var xMenu = document.getElementsByClassName("tabBtn");
  for (i = 0; i < x.length; i++) {
    x[i].style.display = "none";  
    xMenu[i].classList.remove("active"); 
  }
  document.getElementById(tab).style.display = "block";
  document.getElementById(menu).classList.add("active");
}

/* POUR AFFICHER LES DESCRIPTIONS DE VIDEOS */
function showText() {
  var dots = document.getElementById("dots");
  var moreText = document.getElementById("more");
  var btnText = document.getElementById("myBtn");

  if (dots.style.display === "none") {
    dots.style.display = "inline";
    btnText.innerHTML = "Afficher plus"; 
    moreText.style.display = "none";
  } else {
    dots.style.display = "none";
    btnText.innerHTML = "Afficher moins"; 
    moreText.style.display = "inline";
  }
}