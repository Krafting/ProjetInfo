<?php 
include('../init.php');


/* ON CHECK SI LE POST EN ENVOYÉ ET N'EST PAS VIDE  ET SI ON EST CONNECTE*/
if(!connect()) {
	header('Location: ../../');
    exit();
} 
if(isset($_POST['changePass']) 
    AND !empty($_POST['old_pass'])
    AND !empty($_POST['new_pass'])
    AND !empty($_POST['new_pass_conf'])) {
		/* VARIABLE DE MOT DE PASSES */
		$new_pass = hash('sha256',$_POST['new_pass']);
		$new_pass_conf = hash('sha256',$_POST['new_pass_conf']);
		$old_pass_hash = hash('sha256',$_POST['old_pass']);

		$result = $connexion->prepare('SELECT pass FROM users WHERE pseudo=:pseudo');
		$result->execute(array(
			'pseudo' => $_SESSION['pseudo']
		));
		$result2 = $result->fetch(); 
		/* ON CHECK SI L'ANCIEN MOT DE PASS EST CORRECT */
		if ($result2['pass'] == $old_pass_hash) {
			/* ON CHECK SI LES DEUX MOT DE PASSE ENTRÉ CORRESPONDENT */
			if ($new_pass == $new_pass_conf) {
				/* ON HASH LE NOUVEAU PASSWORD */
				$result = $connexion->prepare('UPDATE users set pass=:pass WHERE iduser=:id;');
				$result->execute(array(
					'pass' => $new_pass,
					'id' => $_SESSION['id']
				));
				
				header('Location: ../../membre.php?succ=10');
				exit();
			} else {
				header('Location: ../../membre.php?err=3');
				exit();
			} 	
		} else {
			header('Location: ../../membre.php?err=2');
			exit();
		}
    } else {
		header('Location: ../../membre.php?err=1');
		exit();
    }

   



?>