<?php
include('../init.php');
/* ON CHECK QU'ON EST CONNECTÉ, QUE LE POSTE N'EST PAS VIDE ET ENVOYÉ */
if(connect()) {
    if(isset($_POST['sendComment']) AND isset($_POST['idreceived'])) {
        if(!empty($_POST['contentComment'])) {
            $findUser = $connexion->prepare('SELECT * FROM plays WHERE idvideo=:id');
            $findUser->execute(array(
                'id' => $_POST['idreceived']
            ));
            $findUser = $findUser->fetch();
            /* SI LE PLAY EXISTE, ON AJOUTE LE COMMENTAIRE */
            if(isset($findUser['titrevideo'])) {
                /* ON AJOUTE LE COMMENTAIRE */
                $addComment = $connexion->prepare('INSERT INTO commentairesplay (textecommentaireplay, refusersent, refplay, datecommentaireplay, timecommentaireplay) VALUES (:textecommentaireplay, :refusersent, :refplay, :datecommentaireplay, :timecommentaireplay);');
                $addComment->execute(array(
                    'textecommentaireplay' => $_POST['contentComment'], 
                    'refusersent' => $_SESSION['id'], 
                    'refplay' => $_POST['idreceived'], 
                    'datecommentaireplay' => date('d-m-Y'),
                    'timecommentaireplay' => time()
                ));
                header('Location: ../../watch.php?uuid='.$_POST['idreceived'].'&succ=49');
                exit();

            } else {
                /* LE PLAY N'EXISTE PAS */
                header('Location: ../../watch.php?err=61');
            }
        } else {
            /* MESSAGE D'ERREUR SI LE POST  EST VIDE */
            header('Location: ../../watch.php?uuid='.$_POST['idreceived'].'&err=1');
            exit();
        }
    } else {
        /* SI ON NE PASSE PAS PAR LE POST */
        header('Location: ../../index.php');
        exit();
    }
} else {
    /* SI ON EST PAS CONNECTÉ */
    header('Location: ../../watch.php?uuid='.$_POST['idreceived'].'&err=50');
    exit();
}
?>