<?php
include('../init.php');


if(!connect()) {
	header('Location: ../../');
    exit();
}

/* ON CHECK SI LE POSTE A ÉTÉ ENVOYÉ ET SI LE MAIL EST PAS VIDE */
if( isset($_POST['changeAccount']) AND !empty($_POST['mail'])) {
	$mail = secure($_POST['mail']);
	/* ON CHECK LA REGEX DE L'EMAIL */
	if(preg_match_all($regexmail, $mail)) {
		if(isset($_POST['allowComment']) AND $_POST['allowComment'] == 1) {
			$allowComm = 1;
		} else {
			$allowComm = 0;
		} 
		$nbUser = $connexion->prepare('UPDATE users SET mail=:mail, allowcommentaire=:allowcommentaire WHERE iduser=:id');
		$nbUser->execute(array(
			'mail' => $mail, 
			'allowcommentaire' => $allowComm, 
			'id' => $_SESSION['id']
		));
		header('Location: ../../membre.php?succ=11');
		exit();
	} else {
		header('Location: ../../membre.php?err=4');
		exit();
	}
} else {
	header('Location: ../../membre.php?err=1');
    exit();
}

?>