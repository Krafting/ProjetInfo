<?php 
include('../../init.php');

//si l'utilisateur n'est pas admin et/ou si il n'est pas connecter , on n'affiche pas la page 
if(!connect() OR !isAdmin()) {
	header('Location: ../../../index.php');
    exit();
}

/*quand on appuie sur changer le compte*/

if( isset($_POST['changeAccount'])) {

	/* On verifie si une adresse mail as été rentré*/
	
	if (!empty($_POST['mail']) AND !empty($_POST['rang'])) {
		$regexmail="/[\w\-\.]+@[\w\-\.]+\.[A-z]{2,}$/";
		/* ON CHECK LA REGEX DE L'EMAIL */
		if(preg_match_all($regexmail, $_POST['mail'])) {

			$sql_rang = $connexion->prepare("SELECT count(*) from rangs WHERE idrang=:idrang;");
			$sql_rang->execute(array(
				'idrang' => $_POST['rang'] 
			));
			$sql_rang = $sql_rang->fetch();

			/* on regarde l'etat du allowcomment */
			if(isset($_POST['allowComment']) AND $_POST['allowComment'] == 1) {
				$allowComm = 1;
			} else {
				$allowComm = 0;
			} 

			/* On verifie que le rang choisie existe */
			
			if ($sql_rang[0]>0){
				$sql_mail = $connexion->prepare("UPDATE users set mail=:mail, refrang=:refrang, allowcommentaire=:allowcommentaire where iduser=:iduser;");
				$sql_mail->execute(array(
					'mail' => $_POST['mail'],
					'refrang' => $_POST['rang'],
					'allowcommentaire' => $allowComm,
					'iduser' => $_POST['id']
				));
				header('Location: ../../../admin/users.php?succ=67&id='.secure($_POST['id']));
				exit();
			} else {
				header('Location: ../../../admin/users.php?err=68&id='.secure($_POST['id']));
				exit();
			}
		} else {
			header('Location: ../../../admin/users.php?err=4&id='.secure($_POST['id']));
			exit();
		}
	}  else {
		header('Location: ../../../admin/users.php?err=1&id='.secure($_POST['id']));
		exit();
	}
} else {
	header('Location: ../../../admin/users.php');
    exit();
}
?>