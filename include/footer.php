
    <footer>
        <div class="content">
            <div class="footerLeft">
                <h2>Informations</h2>
                Outbreaker e-Sport (C) 2021 - 2022
                <br/>
                On est la pour vous faire kiffer en fait!
            </div>
            
            <div class="footerRight">
                <h2>Nous contacter!</h2>
                <p>
                    <b>Adresse de contact</b><br> <i>outbreaker@krafting.net</i>
                </p>
            </div>
        </div>
    </footer>
    <script type="text/javascript" src="include/js/js.js"></script>  
</body>
</html>