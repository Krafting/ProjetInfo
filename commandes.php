<?php
$pageTitle = "Commande";
$needConnect = true;
include('include/init.php');
include('include/header.php');

/* SI UN id NUMERIC EST DONNÉ ALORS ON AFFICHE LA PAGE */
if(!empty($_GET['id']) && isset($_GET['id']) && is_numeric($_GET['id'])) {

/* ON VA CHERCHER LES DONNÉES DANS LA BDD, CELA VERIFIE EN MEME TEMPS SI LA COMMANDE APPARTIENT A L'UTILISATEUR */
$reqSelectCommande2 = $connexion->prepare('SELECT count(*) FROM commandes INNER JOIN etats ON refetat=idetat WHERE refuser=:iduser AND idcommande=:idcmd');
$reqSelectCommande2->execute(array(
    'iduser' => $_SESSION['id'],
    'idcmd' => $_GET['id']
));
$count = $reqSelectCommande2->fetch();

if($count[0] > 0) {

    $reqSelectCommande = $connexion->prepare('SELECT * FROM commandes INNER JOIN etats ON refetat=idetat WHERE refuser=:iduser AND idcommande=:idcmd');
    $reqSelectCommande->execute(array(
        'iduser' => $_SESSION['id'],
        'idcmd' => $_GET['id']
    ));
    $commande = $reqSelectCommande->fetch();
    /* SI L'UTILISATEUR ANNULE SA COMMANDE */
        if(!empty($_GET['cancel']) and isset($_GET['cancel']) && is_numeric($_GET['cancel'])) {
            if($commande['refetat'] == 0 OR $commande['refetat'] == 1) {
                $reqSetCommande = $connexion->prepare('UPDATE commandes SET refetat=:etat WHERE idcommande=:idcmd');
                $reqSetCommande->execute(array(
                    'etat' => 5,
                    'idcmd' => $_GET['id']
                ));
                header('Location: ?id='.$_GET['id'].'&succ=54');
                exit();
            } else {
                header('Location: ?id='.$_GET['id'].'&err=53');
                exit();
            }
        }
?>

    <div class="content">
        <div class="page cmd">
            <?php 
            if(isset($_GET['err']) OR isset($_GET['succ'])) {
                if(!empty($_GET['err']) AND is_numeric($_GET['err'])) {
                    $idMsg = $_GET['err'];
                    echo getMessage($idMsg);
                }
                if(!empty($_GET['succ']) AND is_numeric($_GET['succ'])) {
                    $idMsg = $_GET['succ'];
                    echo getMessage($idMsg);
                }
            } ?>
            <h1>Informations à propos de votre commande</h1>
            <table>
                <tr>
                    <td>Commande N°<?php echo $commande['idcommande']; ?></td>
                    <td>Etat : <?php echo $commande['nometat']; ?></td>
                </tr>
                <tr>
                <?php if($commande['refetat'] == 0 OR $commande['refetat'] == 1) { ?>
                    <td>Votre commande est encore en cours de traitement, vous pouvez l'annuler</td>
                    <td>
                        <a href="?id=<?php echo $_GET['id']; ?>&cancel=1">Annuler ma commande</a>
                    </td>
                <?php } elseif($commande['refetat'] == 5) { ?>
                    <td colspan="2">Votre commande a été annulée.</td>
                <?php } else { ?>
                    <td colspan="2">Votre commande a été expédiée, vous ne pouvez plus l'annuler</td>
                <?php } ?>
                </tr>
            </table>
            <br>
            <hr>
            <br>
            <table>
                <tr>
                    <td colspan='2'>Adresse de livraison et de facturation</td>
                </tr>
                <tr>
                    <td style="text-align: left;">
                    <?php echo $commande['villecommande']; ?> (<?php echo $commande['codepostal']; ?>) <br>
                    <?php echo $commande['depcommande']; ?><br>
                    <?php echo $commande['addresscommande']; ?>
                    </td>
                    <td style="text-align: right; vertical-align: top; font-weight: bold">
                        <?php echo $commande['nomcommande']; ?> <?php echo $commande['prenomcommande']; ?> <br>
                        <?php echo $commande['telcommande']; ?>
                    
                    </td>
                </tr>
                <tr>
                    <td colspan='2'>
                        Commentaire :
                        <?php if(empty($commande['extras'])) { 
                            echo "Aucun commentaire n'a été laissé sur votre commande.";  
                        } else { 
                            echo $commande['extras']; 
                    } ?></td>
                </tr>
            </table>
            <br>
            <hr>
            <br>
            <table>
                <tr>
                    <td colspan='3'>Les articles</td>
                </tr>
                <tr>
                    <td style="font-weight: bold; width: 5%">Quantité</td>
                    <td style="font-weight: bold">Nom de l'article</td>
                    <td style="font-weight: bold">Prix</td>
                </tr>
                <?php 
                /* ON VA CHERCHER LES ARTICLES QUI APPARTIENNE A LA COMMANDE */
                $reqSelectArticles = $connexion->prepare('SELECT * FROM commander INNER JOIN articles ON refarticle=idarticle WHERE refcommande=:idcmd');
                $reqSelectArticles->execute(array(
                    'idcmd' => $_GET['id']
                ));
                $articles = $reqSelectArticles->fetchAll();
                foreach($articles as $article) {
                    echo "
                    <tr>
                        <td>".$article['qtecommande']." x</td>
                        <td>".$article['nomarticle']." <a href='article.php?id=".$article['idarticle']."' target='blank'>Lien</a></td>
                        <td>".$article['prixvente']*$article['qtecommande']." € <small style='margin-left: 5px'> (".$article['prixvente']."€ l'unité)</small></td>
                    </tr>";
                }
                
                $result = $connexion->prepare('SELECT sum(prixvente*qtecommande) FROM commander INNER JOIN articles ON refarticle=idarticle WHERE refcommande=:idcmd');
                $result->execute(array(
                    'idcmd' => $_GET['id']
                ));
                $result = $result->fetch();
                echo "<tr>
                            <td colspan=2 style='text-align: right;'> 
                                Prix d'achat total de votre commande.
                            </td>
                            <td>".round($result[0], 2)." €</td>
                        </tr>"; ?>
            </table>
            <br><br>
            <!-- LE CONTENU DE LA PAGE VA APRÈS CECI : -->
        </div>
    </div>

<?php } else { 
    header('Location: membre.php');
    exit();
    } 
} else {
    header('Location: membre.php');
    exit();
}
include('include/footer.php');
?>