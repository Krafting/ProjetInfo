<?php
$pageTitle = "Réinitialisation";
include('include/init.php');
if(connect() == true) {
    header('Location: index.php');
    exit();
}
include('include/header.php');

if(isset($_GET['reset']) and preg_match('@^[a-f0-9]+$@', $_GET['reset'])) {
    $selectUser = $connexion->prepare('SELECT COUNT(*) FROM users WHERE resetlink=:resetlink AND :secondtime<resetdate AND resetdate<:resetdate ');
    $selectUser->execute(array(
        'resetlink' => $_GET['reset'],
        'resetdate' => time(),
        'secondtime' => time()-7200
    ));
    $count = $selectUser->fetch();
    if($count[0] == 1) {
    ?>
    <div class="content">
        <div class="page">
         <?php 
            if(isset($_GET['err']) OR isset($_GET['succ'])) {
                if(!empty($_GET['err']) AND is_numeric($_GET['err'])) {
                    $idMsg = $_GET['err'];
                    echo getMessage($idMsg);
                }
                if(!empty($_GET['succ']) AND is_numeric($_GET['succ'])) {
                    $idMsg = $_GET['succ'];
                    echo getMessage($idMsg);
                }
            } ?>
            <h1>Réinitialiser votre mot de passe.</h1>
            <form method="post" action="include/forms/changeLostPassword.php">         
                <div id="form">
                    <p>Votre nouveau mot de passe </p> 
                    <input type="hidden" name="reset" value="<?php echo $_GET['reset']; ?>">
                    <input type="password" name="new_pass" placeholder="Votre nouveau mot de passe">
                    <p>Confirmer votre nouveau mot de passe </p> 
                    <input type="password" name="new_pass_conf" placeholder="Confirmer votre nouveau mot de passe">

                    <div class="sendButton">
                        <button type="submit" name="changeNewPass" class="btn">Changer le mot de passe</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
<?php } else { header('Location: lostPassword.php'); exit();
} } else { ?>
    <div class="content">
        <div class="page">
            <h1>Vous pouvez demander un lien de réinitialisation de votre mot de passe.</h1>
            <?php 
            if(isset($_GET['err']) OR isset($_GET['succ'])) {
                if(!empty($_GET['err']) AND is_numeric($_GET['err'])) {
                    $idMsg = $_GET['err'];
                    echo getMessage($idMsg);
                }
                if(!empty($_GET['succ']) AND is_numeric($_GET['succ'])) {
                    $idMsg = $_GET['succ'];
                    echo getMessage($idMsg);
                }
            } ?>
            <form method="post" action="include/forms/changeLostPassword.php">         
                <div id="form">
                    <p>Votre adresse mail </p>  
                    <input type="text" name="mail" placeholder="Adresse mail">
                    <small>Vous connaissez votre mot de passe ? <a href="login.php" title="Connexion">Connectez-vous</a></small><br>
                    <br>
                    <div class="sendButton">
                        <button type="submit" name="sendLink" class="btn">Réinitialiser mon mot de passe</button>
                    </div>
                </div>
            </form>
        </div>
    </div>

<?php }
include('include/footer.php');
?>
