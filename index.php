<?php
$pageTitle = "Accueil";
include('include/init.php');
include('include/header.php');

?>

    <div class="content homepage">
        <div class="index">
            <div class="index-content">
                <h1>About the team</h1>
                <p class="small-text">
                    Outbreaker est une petite organisation française, nous avons des équipes dans les jeux
                    les plus populaires d'aujourd'hui !
                </p>
                <ul class="games">
                    <li>
                        <img alt="csgo" src="images/home/csgo.jpg">
                        <h3>Counter-Strike: Global Offensive</h3>
                        <p>
                            Notre équipe CS:GO est la première équipe portant le nom de Outbreaker !
                        </p>
                    </li>
                    <li>
                        <img alt="csgo" src="images/home/lol.jpg">
                        <h3>League of Legends</h3>
                        <p>
                            Nous avons récement engagé une équipe league of legend, bientôt dans les tournois!
                        </p>
                    </li>
                    <li>
                        <img alt="csgo" src="images/home/steam.jpg">
                        <h3>Et plus..</h3>
                        <p>
                            Nous espérons nous agrandir dans le futur, avec plus d'équipes dans plus de jeux différents!
                        </p>
                    </li>
                </ul>
            </div>
        </div>
    </div>


    <div class="content homepage wide-page">
        <div class="index">
            <div class="index-content blue-page">
                <h1>Outbreaker Plays</h1>
                <!-- ON AFFICHE LA SELECTION DU CHEF -->
                <div class="content">
                    <div class="page plays">
                        <h2>Les plays sont des vidéos courtes montrant un gameplay de jeu vidéo, souvent épic!</h2>
                        <div class="flexPlay">

                            <?php 

                            $searchAllPlays = $connexion->prepare('SELECT * FROM plays INNER JOIN users ON refuser=iduser WHERE homepage=1 ORDER BY RAND() DESC LIMIT 6 OFFSET 0');
                            $searchAllPlays->execute();
                            $searchAllPlays->execute();
                            
                            foreach($searchAllPlays as $play) {
                            ?>
                            <div class="previewPlay">
                                <a href="watch.php?uuid=<?php echo $play['idvideo']; ?>">
                                    <img src="../upload/thumbs/<?php echo $play['refuser'].'/'.$play['miniature']; ?>"></img>
                                </a>
                            </div>
                            <?php } ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
        
    <div class="content homepage">
        <div class="index">
            <div class="index-content">
                <h1>Nous recrutons!</h1>
                <p class="small-text">
                    Outbreaker est une organisation eSport en pleine extension, et nous cherchons à 
                    recruter de nouveaux joueurs pour nous aider à grandir et gagner plus de tournois!
                </p>

                <a href="<?php if(!connect()) { echo "register.php?redir=join"; } else { echo "join.php"; }?>" class="btn">jouer avec nous!</a>

            </div>
        </div>
    </div>
<?php
include('include/footer.php');
?>
